"use client";

import { motion } from "framer-motion";

interface AnimatedScreenSectionProps {
  center?: boolean;
  id?: string;
  height?: "auto" | "screen";
  children: React.ReactNode;
}

export default function AnimatedScreenSection({
  center,
  id,
  height = "screen",
  children,
}: AnimatedScreenSectionProps) {
  return (
    <motion.section
      className={`h-auto md:h-${height} w-full ${center ? "py-12 flex flex-col justify-center items-center" : ""}`}
      id={id}
      initial={{ opacity: 0, scale: 0.7, x: -100 }}
      viewport={{ amount: 0.2 }}
      whileInView={{ opacity: 1, scale: 1, x: 0 }}
    >
      {children}
    </motion.section>
  );
}
